import { Artwork, objectIDs } from "./types";

export const getArtWorksIDs = async (): Promise<objectIDs | undefined> => {
  try {
    const IDs = await fetch(
      "https://collectionapi.metmuseum.org/public/collection/v1/search?departmentId=11&q=cat&hasImages=true"
    )
      .then((response) => response.json())
      .then((data) => data.objectIDs ?? {});
    return IDs;
  } catch (e) {
    console.error(e);
  }
};

export const getArtWork = async (
  id: number | undefined
): Promise<Artwork | undefined> => {
  if (!id) return;
  try {
    const artWork = await fetch(
      `https://collectionapi.metmuseum.org/public/collection/v1/objects/${id}`
    )
      .then((response) => response.json())
      .then((data) => data ?? {});
    return artWork;
  } catch (e) {
    console.error(e);
  }
};
