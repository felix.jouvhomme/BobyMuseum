import { useEffect, useState, useCallback } from "react";
import { getArtWork, getArtWorksIDs } from "../Utils/commonQueries";
import { random } from "lodash";
import { Artwork, UseArtWorkResult } from "../Utils/types";

const useArtWork = (): UseArtWorkResult => {
  const [artWork, setArtWork] = useState<Artwork | undefined>();
  const [artWorkLoading, setArtWorkLoading] = useState<boolean>(true);
  const [triggerFetch, setTriggerFetch] = useState<boolean>(false);

  const loadArtWork = useCallback(async () => {
    let randomArtWorkIndex: number, randomId: number, randomArtWork: Artwork;
    setArtWorkLoading(true);
    const artWorksIDs = await getArtWorksIDs();
    randomArtWorkIndex = random(artWorksIDs?.length! - 1);
    randomId = artWorksIDs?.[randomArtWorkIndex];
    randomArtWork = await getArtWork(randomId);
    setArtWork(randomArtWork);
    setArtWorkLoading(false);
  }, []);

  useEffect(() => {
    loadArtWork();
  }, [triggerFetch, loadArtWork]);

  const fetchNewArtwork = () => {
    setTriggerFetch((prevState) => !prevState);
  };

  return { artWork, artWorkLoading, fetchNewArtwork };
};

export default useArtWork;
