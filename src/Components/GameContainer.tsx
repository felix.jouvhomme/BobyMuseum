import { Flex, Image, Text } from "@chakra-ui/react";
import { useState } from "react";
import useArtWork from "../Hooks/useArtWorks";
import GameInput from "./GameInput";
import GameSpinner from "./GameSpinner";
import PostGameMessage from "./PostGameMessage";

const GameContainer = () => {
  const [answer, setAnswer] = useState<string>("");
  const [answerSent, setAnswerSent] = useState<boolean>(false);
  const [postGameMessage, setPostGameMessage] = useState<string>("");

  const { artWork, artWorkLoading, fetchNewArtwork } = useArtWork();

  const correctAnswer = artWork?.objectEndDate === +answer;

  const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setAnswer(event.target.value);
  };

  const handleNewGame = () => {
    setAnswerSent(false);
    setAnswer("");
    fetchNewArtwork();
  };

  const handleAnswer = (answer: string) => {
    if (correctAnswer) {
      setPostGameMessage(
        "Bravo, vous avez trouvé l'année exacte de création de cette oeuvre !"
      );
    } else {
      if (artWork?.objectEndDate > +answer) {
        let difference = artWork?.objectEndDate - +answer;
        setPostGameMessage(
          `Vous avez estimé cette oeuvre avec un écart de -${difference} ans`
        );
      } else {
        let difference = +answer - artWork?.objectEndDate;
        setPostGameMessage(
          `Vous avez estimé cette oeuvre avec un écart de +${difference} ans`
        );
      }
    }
    setAnswerSent(true);
  };

  return (
    <>
      {artWorkLoading ? (
        <GameSpinner />
      ) : (
        <>
          <Flex
            minWidth="max-content"
            alignItems="center"
            flexDirection="column"
          >
            <Text fontSize="3xl" maxWidth="500px">
              {artWork?.title}{" "}
            </Text>
            <Image
              src={artWork?.primaryImage}
              boxSize="500px"
              objectFit="contain"
            />
            {answerSent ? (
              <PostGameMessage
                correctAnswer={correctAnswer}
                handleNewGame={handleNewGame}
                postGameMessage={postGameMessage}
              />
            ) : (
              <GameInput
                answer={answer}
                handleAnswer={handleAnswer}
                handleChange={handleChange}
              />
            )}
          </Flex>
        </>
      )}
    </>
  );
};

export default GameContainer;
