import { Flex, Spinner, Text } from "@chakra-ui/react";

const GameSpinner = () => {
  return (
    <Flex minWidth="max-content" justifyContent="center" marginTop="300">
      <Text marginRight="30px" fontSize="3xl">
        Chargement de l'oeuvre...
      </Text>
      <Spinner
        thickness="4px"
        speed="0.65s"
        emptyColor="gray.200"
        color="blue.500"
        size="xl"
      />
    </Flex>
  );
};

export default GameSpinner;
