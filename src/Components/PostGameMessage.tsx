import { ArrowBackIcon, CheckIcon, CloseIcon } from "@chakra-ui/icons";
import { Button, Text } from "@chakra-ui/react";
import React from "react";

const PostGameMessage = ({
  correctAnswer,
  postGameMessage,
  handleNewGame,
}): JSX.Element => {
  return (
    <>
      {correctAnswer ? (
        <CheckIcon color="green.500" boxSize={6} marginTop="25px" />
      ) : (
        <CloseIcon color="red.500" boxSize={6} marginTop="25px" />
      )}
      <Text fontSize="1xl" marginTop="20px">
        {postGameMessage}
      </Text>
      <Button
        leftIcon={<ArrowBackIcon />}
        onClick={() => handleNewGame()}
        margin="30px"
      >
        Rejouer avec une nouvelle image
      </Button>
    </>
  );
};

export default PostGameMessage;
