import { Button, Input, InputGroup, InputRightAddon } from "@chakra-ui/react";

type GameInputProps = {
  handleChange: (event: React.ChangeEvent<HTMLInputElement>) => void;
  answer: string;
  handleAnswer: (answer: string) => void;
};

const GameInput = ({
  handleChange,
  answer,
  handleAnswer,
}: GameInputProps): JSX.Element => {
  return (
    <InputGroup justifyContent={"center"} marginTop="25px">
      <Input
        type="number"
        onChange={handleChange}
        value={answer}
        placeholder="Guess the date here"
        width="auto"
        focusBorderColor="blue.400"
      />
      <InputRightAddon
        children={<Button onClick={() => handleAnswer(answer)}>Go</Button>}
      />
    </InputGroup>
  );
};

export default GameInput;
