import { Flex, Image, Spacer, Text } from "@chakra-ui/react";
import Logo from "../assets/images/logo_boby_museum.png";

const Header = () => {
  return (
    <>
      <Flex minWidth="max-content" alignItems="center" gap="2" p={5}>
        <Spacer />
        <Text fontSize="6xl" marginRight={5}>
          Boby Museum
        </Text>
        <Image
          marginTop="4"
          boxSize="150"
          objectFit="cover"
          src={Logo}
          alt="Boby Museum logo"
        />
      </Flex>
    </>
  );
};

export default Header;
