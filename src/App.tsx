import { ChakraProvider, theme } from "@chakra-ui/react";
import Header from "./Components/Header";
import GameContainer from "./Components/GameContainer";

export const App = () => (
  <ChakraProvider theme={theme}>
    <Header />
    <GameContainer />
  </ChakraProvider>
);
